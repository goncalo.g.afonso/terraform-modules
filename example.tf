data "aws_ami" "cloud_cobus" {
  most_recent = true
}

module "my_ec2_instance" {
  source = "./new_module"

  ec2_istance_type    = var.ec2_instance_type
  ec2_instance_name   = var.ec2_instance_type
  number_of_instances = var.number_of_instances
  ec2_ami_id          = data.aws_ami.cloud_cobus.id
}